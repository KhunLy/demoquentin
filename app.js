const express = require('express');
const {spawn} = require('child_process');
const app = express();
const port = 3000;


const cors = require('cors');
app.use(express.json());
app.use(cors({
    origin: '*'
}));



app.post('/processPython', (req, res) => {

    //let dataToSend = JSON.stringify(req.query); // si les données sont envoyées en get
    //ou 
    console.log(req.body);
    let dataToSend = JSON.stringify(req.body); // si les données sont envoyées en post

    
    // appelle le script python en passant des arguments
    const python = spawn('python', ['scripts/python.py', dataToSend]);

    // si ok
    python.stdout.on('data', function(data) {
        console.log(data.toString());
        res.json(JSON.parse(data));
    });

    // si pas ok
    python.stderr.on('data', function(data) {
        console.error(data.toString());
        res.statusCode = 500;
    });
})

app.get('/salut', (req, res) => {1
    res.json("Hello");
})
app.listen(port, () => console.log(`Example app listening on port ${port}!`))