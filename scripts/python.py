import sys
import json

## récupérer les données envoyées à python
data = json.loads(sys.argv[1])

def carre(nb):
    return int(nb) * int(nb)

newData = { "carreVal1": carre(data["val1"]), "carreVal2": carre(data["val2"]) }

print(json.dumps(newData))